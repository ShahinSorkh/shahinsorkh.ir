---
date: "2019-07-30 00:49:51 +0430"
title: "More sanctions and open-source community"
---

# [GitHub has restricted all Iranians accounts][sanc-github-t]

Here is the email I had received:

>Due to U.S. trade controls law restrictions, your GitHub account has been
restricted. For individual accounts, you may have limited access to free GitHub
public repository services for personal communications only. Please read about
[GitHub and Trade Controls][sanc-github-tc] for more information. If you believe
your account has been flagged in error, please [file an appeal][sanc-github-ap].

Oh, thank you GitHub letting me know. But what happened to my data? It isn't
going to be another Slack, is it? **I'm afraid, _it is_!** You believe that?!
The greatest open-source advocate is doing this!


[sanc-github-ap]: https://airtable.com/shrGBcceazKIoz6pY
[sanc-github-t]: https://twitter.com/natfriedman/status/1155311121038864384
[sanc-github-tc]: https://help.github.com/articles/github-and-trade-controls
[sanc-github-hn]: https://news.ycombinator.com/item?id=20552797
