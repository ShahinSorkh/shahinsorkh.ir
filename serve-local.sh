#!/bin/sh

set -e

bundle exec jekyll build
python -m http.server -d _site 4000
