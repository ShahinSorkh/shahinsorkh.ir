---
title: My Resume
primary: true
---
# Ali Zakeri (Shahin Sorkh)

## Senior Backend Developer

Karaj, Iran | +989390805086 | sorkh.shahin@hotmail.com | [LinkedIn](https://www.linkedin.com/in/shahinsorkh) | [GitHub](https://github.com/ShahinSorkh)

---

## Summary

Highly skilled Senior Backend Developer with a strong track record in designing and implementing innovative solutions. Collaborated with diverse startup teams and led cross-functional groups to deliver high-quality software products. Experienced in utilizing various technologies and frameworks, such as NestJS, Laravel, Django, Java Spring Boot, and more. Proficient in database management, big data processing, web scraping, and team collaboration. Committed to delivering scalable and reliable solutions to meet project requirements and enhance user experiences. A lifelong learner who actively seeks out opportunities to expand skills and stays updated with the latest industry trends.

---

## Work Experience

### Senior Backend Developer

**Agileful** | 08/2022 - Present (10m) | Remote (Berlin, Germany)

- Collaborated with diverse startup teams, leveraging various technologies to develop and maintain innovative products.
- Worked on various projects, including a dynamic-form based CMS for tour leaders using NestJS, Molecular, and MongoDB, in a team of 6 developers and designers.
- Designed and implemented high-quality software solutions to meet project requirements and align with the frontend team.
- Utilized AWS ESC, S3, and CodePipeline for scalability and seamless deployment.
- Developed and deployed an API within a WooCommerce instance to support purchases via PayPal, SEPA, Stripe, GooglePay, and ApplePay.
- Created REST API endpoints for mobile application integration within the WordPress theme.

### Senior Backend Developer

**Malltina** | 05/2020 - 08/2022 (2y 3m) | Karaj, Iran

- Improved code quality with a test coverage of over 40%, ensuring a reliable and robust codebase.
- Led a team of 3 developers focusing on web scraper and product matching projects, while overseeing a cross-functional team of 8 developers working on different projects.
- Spearheaded the development of a product search project based on Elasticsearch and fuzzy text matching.
- Utilized Laravel, Django, and Celery frameworks, along with MySQL, MongoDB, and RabbitMQ, as the primary stack.
- Mentored and recruited 3 highly talented candidates during a month-long internship program.
- Integrated Sentry into the frontend codebase, facilitated the migration from React.js to Next.js, and provided ongoing support for deploying and maintaining various services.
- Applied the principles of Domain-Driven Design (DDD) for enhanced scalability and maintainability in new projects.

### Backend Lead

**Meshkan Limited** | 05/2019 - 05/2020 (1y) | Tehran, Iran

- Led a team of 4 developers as the Backend Lead for the social media platform [Pinno](https://pinno.app), combining the best features of Instagram, TikTok, and Pinterest.
- Developed features using Java Spring Boot, Apache Cassandra, Elasticsearch, Redis, Apache Spark, and Apache Flink.
- Implemented GitLab CI/CD pipelines, automated deployments, and ensured code quality.
- Achieved response times below 60ms, deployed and integrated services including Spark batch processing tasks, Flink stream processing tasks, and an encrypted near-realtime direct chat feature.

### Full Stack Developer

**Douran GROUP** | 03/2017 - 05/2019 (2y 2m) | Tehran, Iran

- Designed and implemented a scalable image dataset classifier with Laravel and VueJs. Utilized job queues for frame extraction from videos, supporting multi-choice operations on the filesystem and nested categories. Ensured automatic backup of image files.
- Developed an AI showcase webpage using Laravel, VueJS, and Vuetify, featuring face recognition as a demonstration. The webpage connected to PostgreSQL for real-time updates and provided functionality for merging duplicate faces and names.
- Built a highly available, rapid dashboard with Laravel and VueJS to query and operate on a database of 150M+ records stored in Apache Cassandra and Elasticsearch. Achieved response times below 100ms. Implemented ACL for access control, dynamic BI-like reports, and integration with external services via HTTP APIs.
- Created a fully customizable web scraper using Laravel, VueJS, and Docker Swarm. Developed a dashboard panel for configuring spiders to scrape websites, including JS-rendered pages. Automated the management of scraper instances on multiple Ubuntu VMs using a dedicated dashboard, optimizing efficiency and scalability.
- Assisted junior developers in performing their tasks with the best performance and efficiency.
- Conducted classes on Git and Docker, imparting knowledge and skills to team members.

---

## Skills

- **Programming Languages:** TypeScript (NestJS, TypeORM), PHP (Laravel), Python (Flask, Django, Scrapy), Java (Spring Boot)
- **Databases:** MySQL, MongoDB, Elasticsearch, Cassandra, Redis
- **Big Data Processing:** Kafka, Spark, Flink
- **Web Scraping:** Scrapy framework, data extraction, parsing, and automation
- **Team Collaboration:** Effective communication, cross-functional teamwork, mentorship
- **Others:** RegEx, Docker, GitLab CI/CD

---

## Languages

- English (Professional Working Proficiency)
- Persian (Native or Bilingual Proficiency)
