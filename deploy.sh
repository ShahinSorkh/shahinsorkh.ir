#!/bin/sh

set -e

cd "$(dirname "$0")"

git reset --hard
git pull
docker pull ruby:3.0
docker run --rm -it -v '/var/www/blog':/blog -w /blog ruby:3.0 sh -c 'gem install bundler:2.4.13; bundle config set --local path vendor; bundle install; bundle exec jekyll build -d public'
