FROM jekyll/builder:3.8.5 AS builder

USER root:root
COPY Gemfile Gemfile.lock ./
RUN  bundle install


FROM jekyll/minimal:3.8.5

COPY --from=builder /usr/local/bundle /usr/local/bundle
COPY --chown=jekyll . .

ENTRYPOINT ["bundle", "exec", "jekyll", "serve", "--host", "0.0.0.0", "-d", "/var/jekyll/blog"]
