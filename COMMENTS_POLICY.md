This place is kinda my home. I don’t know what your home is like, but I know how I expect people to behave when they visit mine. That’s why I reserve the right to delete comments and ban users as needed to keep the comment threads here civil and substantive.

My No. 1 house rule is simple: *Don’t be a jerk.*

Want to be the kind of commenter we’d love to bring home to Thanksgiving dinner? Here’s what we like to see in comments:

- Weigh in with smart, informed ideas that contribute further to the story.
- Give me useful, constructive criticism. Spot a typo or an error? Let me know and I will correct it.
- Demonstrate and share the intelligence, wisdom, and humor I know you possess.
- Don’t feed the trolls. You wouldn’t dive into a debate with our ill-informed, weird uncle Gary just for the heck of it. And you definitely wouldn’t feed him. Downvote and flag comments instead.

Although I can’t be everywhere at once, here are some of the kinds of comments I'm going to do my best to curtail:

- Promoting your own brand, product, or blog. So you’ve got a climate change solution that will simultaneously solve world poverty. Great. But I'm afraid, this is not where you are looking for.
- Impersonating authors or other commenters. I can’t believe I have to say this, but: Don’t do that. It’s weird.
- Comments that make it clear you didn’t read the article. Enraged that I didn’t mention X in a story about Y? Slow down, Speedy McFingerson. If you’d made it past paragraph two, you’d see a very well thought-out discussion of that X you hold so dear.
- Comments that are completely out of left field. Sometimes discussions veer off a bit, but are still related to the original subject. That is fine. Hijacking the conversation to promote off-topic commentary is not.
- Threats — no matter how vague — against me or other commenters. Things can get heated. Before you casually mention your foe’s home address, think of your Mother Earth. (Bonus points if you never use the phrase “Mother Earth.”)
- Racism, sexism, homophobia, you get the drift. Call me the PC Police, fine, but don’t say I didn’t warn you when you get tased. **And by tased, I mean banned or deleted. I have not — and will not — ever own a taser, and even if I was to own a solar-charged taser, I’d be too scared to use it.
- Trolling. If you’re a climate denier just out for a good trolling and are not contributing meaningfully to the conversation, I’ll be pushing you back under the bridge.


Copied and rephrased from [Grist Comment Policy](https://grist.org/grist-comment-policy/)
